-- SETTINGS
NEW_STYLE_WIRES  = true  -- true = new nodebox wires, false = old raillike wires
PRESSURE_PLATE_INTERVAL = 0.04
PISTON_MAXIMUM_PUSH = 12
